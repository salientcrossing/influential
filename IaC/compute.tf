
# resource "aws_instance" "jenkins-instance" {
#   count                  = var.az_count
#   ami                    = data.aws_ami.amazon_linux.id
#   instance_type          = "t3.medium"
#   key_name               = var.key_pair
#   vpc_security_group_ids = [aws_security_group.sg_allow_ssh_jenkins.id]
#   subnet_id              = cidrsubnet(aws_vpc.vpc.cidr_block, 3, var.az_count + count.index)
#   user_data              = file("install_jenkins.sh")

#   associate_public_ip_address = true
#   tags = {
#     Name = "Jenkins-Instance"
#   }
# }

resource "aws_instance" "jenkins-instance" {
  count = var.az_count


  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = var.instance_type
  key_name               = var.key_pair
  vpc_security_group_ids = [aws_security_group.sg_allow_ssh_jenkins.id]
  subnet_id              = element(aws_subnet.public.*.id, count.index)
  user_data              = file("install_jenkins.sh")

  associate_public_ip_address = true
  tags = {
    Name = "Jenkins-Instance"
  }
}


resource "aws_security_group" "sg_allow_ssh_jenkins" {
  name        = "allow_ssh_jenkins"
  description = "Allow SSH and Jenkins inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = local.ssh_from
    to_port     = local.ssh_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = local.http_from
    to_port     = local.http_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# output "jenkins_ip_address" {
#   value = aws_instance.jenkins-instance.public_dns
# }