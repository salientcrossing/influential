
locals {
  db_to   = 3306
  db_from = 3306

  ssh_to   = 22
  ssh_from = 22

  http_to   = 80
  http_from = 80

}