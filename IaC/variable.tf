
variable "aws_cidr" {
  type    = string
  default = "192.168.0.0/16"
}

variable "env" {
  type    = string
  default = "DPC"
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "key_pair" {
  type    = string
  default = "congo"
}

variable "instance_type" {
  type = string
  default = "t3.medium"
}